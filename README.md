# Lancer le jeu

Le jeu a été créé grâce à Java 8, il est donc nécessaire de posséder au moins la version 8 du JDK pour compiler le jeu (à partir de la version 11, JavaFX n'étant plus inclus dans Java, il est nécessaire de l'installer manuellement).

Les instructions suivantes permettent de compiler puis d'exécuter le jeu avec les versions 8, 9 et 10 de Java :
- se placer à la racine du projet
- ```cd src_basic```
- ```javac -d ../out -encoding utf-8 Game/Main.java```
- ```cd ..```
- déplacer les dossiers ```css``` et ```images``` (contenus dans le répertoire ```resources```) dans le dossier ```out```
- ```cd out```
- ```java Game.main```

Ensuite, à chaque fois qu'il faut lancer le jeu, il suffit juste de faire la dernière commande (en étant dans le dossier ```out```).

# Description du jeu

Le jeu se déroule dans un royaume composé de plusieurs châteaux. L'objectif pour le joueur est de prendre le contrôle des châteaux adverses afin de conquérir le royaume.

![screenshot](screenshot.png)

## Déroulement

Le jeu est divisé en tours de jeu s'enchaînant toutes les 2 secondes : à chaque tour, chaque château augmente sa richesse et exécute un ordre (s'il en dispose d'un).

Au départ, chaque duc (dont le joueur) ne possède qu'un seul château. Afin de facilement identifier le duc propriétaire d'un château, la couleur de sa porte diffère en fonction du propriétaire :
les châteaux du joueur ont la porte rouge, et les autres châteaux ont des couleurs différentes. Plusieurs châteaux ont également la porte blanche, ce qui signifie qu'ils sont neutres.

Le jeu se termine lorsque le joueur ne possède plus de château (défaite) ou lorsqu'il possède tous les châteaux, y compris les neutres (victoire).

## Les châteaux

Les châteaux possèdent :
- un niveau,
- un trésor (qui augmente de 10\*niveau à chaque tour),
- une réserve de troupes,
- une file d'attente de productions,
- une porte d'entrée.

Si le château n'est pas neutre, il possède aussi une liste d'ordres de déplacement de soldats.

Au début du jeu, les châteaux non neutres ont un niveau égal à 1 et un trésor vide.
Les châteaux neutres, quant à eux, ont un niveau aléatoire entre 1 et 3 (inclus) et un trésor aléatoire entre 0 et 300 florins.

## La réserve et les soldats

Chaque château possède une réserve de soldats. Il existe trois types de soldats : les piquiers, les chevaliers et les onagres. Voici leurs caractéristiques :

|           | Coût production | Temps production | Vitesse | Vie | Dégâts |
|----------:|:---------------:|:----------------:|:-------:|:---:|:------:|
|   Piquier |        50       |         3        |    2    |  1  |    1   |
| Chevalier |       250       |        10        |    5    |  4  |    4   |
|    Onagre |       500       |        20        |    1    |  5  |   10   |

Le coût de production correspond au nombre de florins nécessaires pour produire le soldat.<br>
Le temps de production est le nombre de tours nécessaires pour que la production soit terminée (c'est-à-dire pour que le soldat rejoigne la réserve du château).<br>
La vitesse correspond au nombre de cases sur lesquelles le soldat peut se déplacer à chaque tour (les cases correspondent aux carrés aux bords verts présents sur la fenêtre).

Au début du jeu, les châteaux non neutres contiennent 3 piquiers et 1 chevalier, et les châteaux neutres contiennent 5 piquiers, 2 chevaliers et 1 onagre.

## Les productions

Chaque château possède une file d'attente de productions. Il est possible de produire soit un soldat, soit le niveau suivant du château.

A chaque tour, la première production de la file avance ; lorsque celle-ci est terminée, elle laisse sa place à la production suivante (s'il y en a une) et effectue l'action demandée.
Dans le cas où la production est un soldat, l'action est l'ajout du soldat dans la réserve ; dans l'autre cas, l'action est le passage au niveau supérieur au château.<br>
(La production du niveau suivant du château coûte (500\*niveauActuel) florins et prend (50\*niveauActuel) tours pour être complétée.)

Il est possible d'annuler une production : dans ce cas, le coût de la production est annulé (les florins dépensés reviennent dans le trésor du château).

Si le château est neutre, alors les temps de production sont multipliés par 10.

## Les ordres

Les châteaux non neutres possèdent une liste d'ordres en cours. Ces ordres sont composés d'un château cible, d'une liste de soldats appliquant l'ordre, et d'une vitesse de déplacement (correspondant à la plus petite vitesse des soldats de la liste).

Lorsqu'un ordre est créé, les soldats concernés quittent la réserve de leur château et se déplacent vers la case adjacente à l'entrée de leur château.<br>
Ensuite, à chaque tour, les soldats se déplacent vers l'entrée du château cible à une vitesse correspondant à la vitesse de l'ordre.
Lorsqu'ils arrivent devant l'entrée du château cible, il leur faut un tour de plus pour rentrer dans le château, à la suite de quoi ils effectuent une action en fonction du duc propriétaire de celui-ci :
- si le duc est le même que le duc du château d'origine, alors les soldats rejoignent la réserve du château cible ;
- sinon, les soldats attaquent le château.

Sur la fenêtre, les soldats appartenant à un ordre sont représentés par deux épées croisées et une couleur : cette couleur est la même que la porte du château d'origine (afin de savoir à qui appartiennent les soldats). Cela implique que si le château d'origine change de propriétaire pendant que les soldats se déplacent, alors les soldats changent eux aussi de propriétaire (et leur couleur change en conséquence).

### Déroulement des attaques

Lorsque les soldats se retrouvent dans un château adverse, ils l'attaquent. Dans ce cas, pour chaque point de dégât des soldats attaquants, un point de vie est enlevé à un soldat aléatoire de la réserve du château.
Lorsque le nombre de points de vie ou de points de dégâts d'un soldat est égal à 0, alors ce soldat est détruit (il n'est pas possible de régénérer ses points de vie ou de dégâts).<br>
L'attaque peut prendre fin de deux manières :
- les soldats attaquants sont tous morts, dans ce cas le château cible ne change pas de propriétaire ;
- la réserve du château est entièrement détruite : dans ce cas, si les soldats attaquants ne sont pas tous morts, alors le château devient propriété du duc à l'origine de l'ordre.
  De plus, toutes les productions de la file de productions du château sont annulées.

# Fonctionnalités

## Commandes

Le clic gauche sur un château affiche (à droite de l'écran) des informations sur celui-ci : son nom, son propriétaire, son trésor, son niveau et le contenu de sa réserve.
Si le château appartient au joueur, la liste des productions et des ordres est aussi visible.<br>
Le clic gauche sur une autre case affiche (à droite de l'écran) les commandes du jeu.

Les touches Espace et P permettent de mettre le jeu en pause. Lorsqu'il est en pause, ces mêmes touches permettent de le faire repartir
(note : à la fin d'une pause, le jeu repart au début du tour actuel, ce qui fait que les soldats en déplacement reviennent à l'endroit où ils étaient au début du tour).

La touche Echap permet de quitter le jeu. La partie est alors automatiquement sauvegardée, et le prochain démarrage du jeu lancera cette partie.

Le clic droit sur un château affiche un menu permettant de réaliser les différentes interactions possibles avec celui-ci.

## Interactions avec les châteaux (clic droit)

Chaque château peut être renommé. Leurs noms sont des noms de châteaux existant réellement en Aquitaine, mais il est possible des les renommer à sa guise.

### Château ennemi

Si le joueur fait un clic droit sur un château qui ne lui appartient pas, il a la possibilité de l'attaquer depuis l'un des châteaux qu'il possède.
Après avoir sélectionné le château d'origine, si la réserve de ce château n'est pas vide, alors le joueur est invité à sélectionner le nombre de piquiers, puis de chevaliers, puis d'onagres qu'il souhaite faire attaquer.
Le joueur doit entrer un nombre, sinon un message d'erreur s'affiche. Si le joueur entre un nombre supérieur au nombre de soldats de ce type disponibles dans la réserve (par exemple s'il entre le nombre 5 lors de la sélection des piquiers, alors qu'il ne possède que 3 piquiers), alors le maximum de soldats possibles de ce type sera envoyé (dans l'exemple précédent, 3 piquiers seront envoyés.)
A la fin de la sélection, si le nombre de soldats envoyés n'est pas nul, alors un ordre est créé avec pour cible le château sur lequel le joueur a cliqué, et avec le nombre de soldats demandés.

### Château possédé

Si le joueur fait un clic droit sur un château qui lui appartient, plusieurs interactions sont possibles.

Il peut ajouter une production à la file d'attente des productions du château. Pour cela, il doit choisir un type de production : si le château possède un trésor suffisant, alors la production est ajoutée à la file.

Si la file de productions du château n'est pas vide, il peut annuler une production. Dans ce cas, une boîte de dialogue s'affiche, l'invitant à sélectionner la production à annuler (elles sont affichées dans l'ordre de la file d'attente).
Lorsque la production est annulée, alors le coût nécessaire à celle-ci est annulé et revient dans le trésor du château.

Si le joueur possède plusieurs châteaux, il peut y transférer des troupes depuis l'un des autres châteaux qu'il possède. Pour cela, il doit choisir dans le menu le château d'origine des troupes, et ensuite les sélectionner de la même manière que pour attaquer un château ennemi.

Si la liste des ordres du château n'est pas vide, il peut annuler un ordre. Dans ce cas, une boîte de dialogue s'affiche, l'invitant à sélectionner l'ordre à annuler. Lorsque celui-ci est annulé, alors il existe toujours mais la cible est devenue le château d'origine de l'ordre, et l'ordre ne disparaîtra que lorsque les soldats seront revenus à leur château d'origine.

## Grille de jeu

La fenêtre est découpée en cases carrées délimitées par des traits verts. Chaque château occupe une case, et la distance entre chaque château est supérieure à 5 cases.<br>
Les paramètres liés à la grille de jeu (dimensions de la fenêtre, largeur des cases, nombre de châteaux, etc.) sont modifiables dans le fichier ```Settings.java```.
Cependant, modifier ces paramètres peut empêcher le lancement du jeu : en effet, lorsqu'un château est créé, il choisit une case aléatoire, et tant que celle-ci n'est pas valide (c'est-à-dire une case qui est trop proche d'un autre château déjà créé), alors le château choisit une autre case.
Or, si par exemple le nombre de châteaux est trop élevé, ou la largeur des cases trop grande, alors il peut ne pas y avoir assez de cases valides et donc le programme cherche en boucle une case valide sans en trouver.

## Sauvegarder la partie

Lorsque le joueur quitte le jeu (en appuyant sur Echap), alors la partie est automatiquement sauvegardée dans un fichier ```sauvegarde``` situé dans le dossier où le jeu a été exécuté.
Ainsi, à chaque démarrage du jeu, si un fichier ```sauvegarde``` existe alors celui-ci est chargé, et la partie reprend dans l'état où elle a été laissée ; sinon une nouvelle partie est créée.
Cela signifie qu'il n'est pas possible de démarrer une nouvelle partie sans supprimer ce fichier.

Lorsque le joueur quitte le jeu après une victoire ou une défaite, le fichier ```sauvegarde``` n'est pas mis à jour : cela signifie que lorsque le jeu sera relancé, il reprendra au niveau de la dernière sauvegarde (s'il n'y a pas de sauvegarde, une nouvelle partie sera créée).
C'est aussi le cas si le joueur quitte le jeu en cliquant sur la croix pour fermer la fenêtre.

## Non-intelligence artificielle

Les châteaux adverses sont contrôlés par une intelligence artificielle minimaliste.

A chaque tour, ils produisent (s'ils le peuvent) un piquier. Puis, s'ils possèdent plus de 8 piquiers dans leur réserve (et s'ils ne sont pas neutres), alors ils ont une chance sur 5 d'envoyer 4 piquiers vers un autre château.

## Points à améliorer

Les soldats n'évitent pas les châteaux lorsqu'ils se déplacent. Ils n'évitent pas non plus les soldats adverses.

Lorsqu'un ordre est donné, tous les soldats sortent du château en même temps, alors que d'après la consigne ils sont censés sortir par groupes de 3.<br>
De plus, pour sélectionner les soldats, il faut passer par 3 boîtes de dialogue différentes (correspondant aux 3 types de soldats différents), et le joueur peut écrire ce qu'il veut dans la boîte de dialogue.
Nous aurions préféré tout réunir en une seule boîte, et également ne pas permettre d'écrire autre chose que des nombres positifs, mais nous n'avons pas trouvé comment faire tout cela avec JavaFX.

Lorsque le nombre de châteaux non neutres est supérieur à 8, alors certains ont la même couleur alors qu'ils n'appartiennent pas au même duc.
Cela est lié au fait que nous utilisons des images différentes pour chaque couleur possible, et donc que nous ne pouvions pas créer une infinité d'images pour toutes les couleurs possibles.
Le fait d'utiliser des formes géométriques créées grâce à JavaFX aurait permis d'avoir une infinité de couleurs possibles. Cependant, nous pensons que jouer avec 8 châteaux non neutres est suffisant, c'est pourquoi nous n'avons créé que 8 images.
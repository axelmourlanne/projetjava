package Game.Chateaux;

import Game.*;
import Game.Soldats.Chevalier;
import Game.Soldats.Onagre;
import Game.Soldats.Piquier;
import Game.Soldats.Soldat;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Chateau extends Sprite implements Serializable {

    /**
     * Indique si le château est neutre.
     */
    private boolean neutre;
    /**
     * Le nom du château.
     */
    private String nom;
    /**
     * Le propriétaire du château.
     */
    private Duc duc;
    /**
     * L'argent que possède le château (le nombre de florins du château).
     */
    private int tresor;
    /**
     * Le niveau du château.
     */
    private int niveau;
    /**
     * La réserve des soldats appartenant au château.
     */
    private Reserve reserve;

    /**
     * La file de productions du château.
     */
    private List<Production> productions = new ArrayList<>();
    /**
     * L'orientation de l'entrée du château.
     */
    private Orientation orientation;
    /**
     * La liste des ordres du château.
     */
    private List<Ordre> ordres = new ArrayList<>();

    /**
     * Crée un château.
     * @param layer La fenêtre.
     * @param image L'image du château.
     * @param neutre Indique si le château est neutre ou non.
     * @param nom Le nom du château.
     * @param duc Le duc propriétaire du château.
     * @param caseX La coordonnée x du château.
     * @param caseY La coordonnée y du château.
     * @param orientation L'orientation du château.
     */
    public Chateau(Pane layer, Image image, boolean neutre, String nom, Duc duc, int caseX, int caseY, Orientation orientation) {
        super(layer, image, caseX, caseY, orientation.ordinal());
        this.neutre = neutre;
        this.nom = nom;
        this.duc = duc;
        this.orientation = orientation;
        if (neutre) {
            this.niveau = (int)(Math.random()*3)+1;
            this.tresor = (int)(Math.random()*300);
        } else {
            this.niveau = 1;
            this.tresor = 0;
        }
    }

    /**
     * Crée la réserve du château.
     * @param neutre Indique si le château est neutre.
     */
    public void creerReserve(boolean neutre) {
        if (neutre)
            this.reserve = new Reserve(this, 5, 2, 1);
        else
            this.reserve = new Reserve(this, 3, 1, 0);
    }

    /**
     * Effectue le tour pour le château (produit son revenu et avance la production et les ordres en cours).
     */
    public void effectuerTour() {
        modifierTresor(10*this.niveau);
        if (!productions.isEmpty()) {
            Production p = productionEnCours();
            p.avancer();
            if (p.estTermine()) {
                terminerProduction(p);
            }
        }
        if (this.getDuc() != Main.joueur) {
            if (this.getProductions().isEmpty()) {
                this.ajouterProduction(TypeProduction.PIQUIER);
            }
            if (!neutre && this.getReserve().nombrePiquiers() > 8) {
                int chateau = (int) (Math.random()*(Settings.NB_CHATEAUX+Settings.NB_CHATEAUX_NEUTRES));
                if (Math.random()*5 > 4 && this != Main.chateaux.get(chateau))
                    this.ajouterOrdre(Main.chateaux.get(chateau), 4, 0, 0);
            }
        }
        for (int i = 0; i < ordres.size(); i++) {
            Ordre o = ordres.get(i);
            o.caseSuivante();

            if (o.cibleAtteinte()) {
                o.prendreCible();
            }
        }
    }

    /**
     * Ajoute un soldat à la réserve du château.
     * @param s Le soldat à ajouter.
     */
    public void ajouterSoldat(Soldat s) {
        if (s instanceof Piquier)
            reserve.ajouterPiquier((Piquier) s);
        else if (s instanceof Chevalier)
            reserve.ajouterChevalier((Chevalier) s);
        else
            reserve.ajouterOnagre((Onagre) s);
    }

    /**
     * Supprime un soldat de la réserve du château.
     * @param s Le soldat à supprimer.
     */
    private void supprimerSoldat(Soldat s) {
        if (s instanceof Piquier)
            reserve.supprimerPiquier((Piquier) s);
        else if (s instanceof Chevalier)
            reserve.supprimerChevalier((Chevalier) s);
        else
            reserve.supprimerOnagre((Onagre) s);
    }

    /**
     * Inflige un point de dégât au château.
     */
    public void prendDegat() {
        Soldat s = this.reserve.soldatAleatoire();
        s.prendDegat();
        if (s.getVie() == 0) {
            s.removeFromLayer();
            this.supprimerSoldat(s);
        }
    }

    /**
     * Teste si le château n'a plus d'armée.
     * @return true si l'armée est vide, false sinon.
     */
    public boolean armeeVide() {
        return reserve.reserveVide();
    }

    /**
     * Modifie le trésor possédé par le château.
     * @param dTresor La quantité de florins à ajouter (ou à enlever si le nombre est négatif).
     */
    private void modifierTresor(int dTresor) {
        this.tresor += dTresor;
    }

    /**
     * Ajoute une production à la file de productions du château.
     * @param type Le type de la production.
     * @return true si la production a bien été ajoutée, false sinon.
     */
    public boolean ajouterProduction(TypeProduction type) {
        int facteur = 1;
        if (neutre)
            facteur = 10;
        Production p;
        if (type == TypeProduction.PIQUIER && tresor >= Piquier.coutProduction) {
            p = new Production(TypeProduction.PIQUIER, Piquier.tempsProduction*facteur);
            tresor -= Piquier.coutProduction;
        } else if (type == TypeProduction.CHEVALIER && tresor >= Chevalier.coutProduction) {
            p = new Production(TypeProduction.CHEVALIER, Chevalier.tempsProduction*facteur);
            tresor -= Chevalier.coutProduction;
        } else if (type == TypeProduction.ONAGRE && tresor >= Onagre.coutProduction) {
            p = new Production(TypeProduction.ONAGRE, Onagre.tempsProduction*facteur);
            tresor -= Onagre.coutProduction;
        } else if (type == TypeProduction.CHATEAU && tresor >= this.getCoutProduction()) {
            p = new Production(TypeProduction.CHATEAU, 50*(this.niveau)*facteur);
            tresor -= this.getCoutProduction();
        } else
            return false;
        productions.add(p);
        return true;
    }

    /**
     * Ajoute une production terminée au jeu (ajoute un soldat à la réserve, ou augmente le niveau du château).
     * @param p La production terminée.
     */
    private void terminerProduction(Production p) {
        productions.remove(p);
        TypeProduction type = p.getType();
        if (type == TypeProduction.CHATEAU)
            this.niveau++;
        else {
            if (type == TypeProduction.PIQUIER) {
                Piquier pq = new Piquier(this);
                ajouterSoldat(pq);
            }
            else if (type == TypeProduction.CHEVALIER) {
                Chevalier c = new Chevalier(this);
                ajouterSoldat(c);
            }
            else {
                Onagre o = new Onagre(this);
                ajouterSoldat(o);
            }
        }
    }

    /**
     * Annule une production (en cours ou programmée).
     * @param p La production à annuler.
     */
    public void annulerProduction(Production p) {
        if (p.getType() == TypeProduction.PIQUIER)
            tresor += Piquier.coutProduction;
        else if (p.getType() == TypeProduction.CHEVALIER)
            tresor += Chevalier.coutProduction;
        else if (p.getType() == TypeProduction.ONAGRE)
            tresor += Onagre.coutProduction;
        else
            tresor += this.getCoutProduction();
        productions.remove(p);
    }

    /**
     * Indique le nombre de productions en cours.
     * @return La taille de la liste des productions.
     */
    public int nombreProductions() {
        return productions.size();
    }

    /**
     * Indique les coordonnées de l'entrée du château (la case adjacente à la porte).
     * @return Un tableau de taille 2, contenant la coordonnée x et la coordonnée y.
     */
    public int[] entreeChateau() {
        int caseX = this.caseX, caseY = this.caseY;
        if (orientation == Orientation.NORD) caseY--;
        else if (orientation == Orientation.SUD) caseY++;
        else if (orientation == Orientation.EST) caseX++;
        else if (orientation == Orientation.OUEST) caseX--;
        return new int[]{caseX,caseY};
    }

    /**
     * Indique quelle production est en cours.
     * @return La production en cours (le premier élément de la file de productions).
     */
    private Production productionEnCours() {
        return productions.get(0);
    }

    /**
     * Ajoute un ordre à la liste des ordres du château.
     * @param cible Le château cible de l'ordre.
     * @param nbPiquiers Le nombre de piquiers auxquels l'ordre s'applique.
     * @param nbChevaliers Le nombre de chevaliers auxquels l'ordre s'applique.
     * @param nbOnagres Le nombre d'onagres auxquels l'ordre s'applique.
     */
    public void ajouterOrdre(Chateau cible, int nbPiquiers, int nbChevaliers, int nbOnagres) {
        List<Soldat> soldats = new ArrayList<>();
        for (int i = 0 ; i < nbPiquiers ; i++) {
            Piquier p = this.getReserve().getPiquiers().get(0);
            soldats.add(p);
            this.supprimerSoldat(p);
        }
        for (int i = 0 ; i < nbChevaliers ; i++) {
            Chevalier ch = this.getReserve().getChevaliers().get(0);
            soldats.add(ch);
            this.supprimerSoldat(ch);
        }
        for (int i = 0 ; i < nbOnagres ; i++) {
            Onagre o = this.getReserve().getOnagres().get(0);
            soldats.add(o);
            this.supprimerSoldat(o);
        }
        ordres.add(new Ordre(cible, this, soldats));
    }

    /**
     * Annule un ordre (les soldats reviennent vers le château).
     * @param o L'ordre à annuler.
     */
    public void annulerOrdre(Ordre o) {
        if (o.getSoldats().get(0).collidesWith(this)) {
            supprimerOrdre(o);
            for (Soldat s : o.getSoldats()) {
                ajouterSoldat(s);
            }
        }
        else
            o.setCible(this);
    }

    /**
     * Supprime un ordre (l'ordre n'existe plus).
     * @param o L'ordre à supprimer.
     */
    public void supprimerOrdre(Ordre o) {
        ordres.remove(o);
    }

    /**
     * Change le propriétaire du château.
     * @param nouveauDuc Le nouveau propriétaire.
     */
    public void changerProprietaire(Duc nouveauDuc) {
        neutre = false;
        this.getDuc().supprimerChateau(this);
        this.setDuc(nouveauDuc);
        this.getDuc().ajouterChateau(this);
        Image castleImage = new Image(getClass().getResource(Settings.IMAGES_CHATEAUX[nouveauDuc.getNumero()]).toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true);
        this.setImage(castleImage, orientation.ordinal());
        for (Ordre o : this.ordres) {
            for (Soldat s : o.getSoldats()) {
                Image i = new Image(getClass().getResource(Settings.IMAGES_SOLDATS[nouveauDuc.getNumero()]).toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true);
                s.setImage(i, 0);
            }
        }
        for (int i = nombreProductions()-1 ; i >= 0 ; i--) {
            annulerProduction(productions.get(i));
        }
    }

    /**
     * Indique le coût de production du niveau suivant du château.
     * @return Le coût du niveau suivant du château.
     */
    public int getCoutProduction() {
        return 500*(this.niveau);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Duc getDuc() {
        return duc;
    }

    public void setDuc(Duc duc) {
        this.duc = duc;
    }

    public Reserve getReserve() {
        return reserve;
    }

    public List<Production> getProductions() {
        return productions;
    }

    public List<Ordre> getOrdres() {
        return ordres;
    }

    /**
     * Indique le nombre d'ordres en cours.
     * @return La taille de la liste des ordres.
     */
    public int nombreOrdres() {
        return ordres.size();
    }

    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * Permet d'obtenir les informations sur le château.
     * @param proprietaire Indique si le joueur est propriétaire du château.
     * @return Une chaîne des caractères contenant les informations.
     */
    public String toString(boolean proprietaire) {
        StringBuilder s = new StringBuilder("Duc : " + getDuc().getNom() + "\n\nNiveau " + this.niveau + "\nTrésor : " + this.tresor +
                " florins\n\nRéserve :\n" + this.reserve + "\n");
        if (proprietaire) {
            List<Production> prods = this.getProductions();
            if (prods.isEmpty()) {
                s.append("Aucune production en cours\n");
            } else {
                s.append("Production en cours :\n").append(prods.get(0)).append("(").append(prods.get(0).getTempsRestant()).append(" tours restants)\n");
                if (prods.size() > 1) {
                    s.append("Productions programmées :\n");
                    for (int i = 1; i < prods.size(); i++) {
                        s.append(i).append(". ").append(prods.get(i));
                    }
                }
            }
            s.append("\n");
            List<Ordre> ordres = this.getOrdres();
            if (ordres.isEmpty()) {
                s.append("Aucun ordre en cours\n");
            } else {
                s.append("Ordres en cours :\n");
                for (Ordre ordre : ordres) {
                    s.append(ordre);
                }
            }
        }
        return s.toString();
    }
}

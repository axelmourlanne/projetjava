package Game.Chateaux;

import Game.Soldats.Soldat;

import java.io.Serializable;
import java.util.List;

public class Ordre implements Serializable {
    /**
     * Le château cible de l'ordre.
     */
    private Chateau cible;
    /**
     * Le château à l'origine de l'ordre.
     */
    private Chateau origine;
    /**
     * La liste des soldats auxquels l'ordre s'applique.
     */
    private List<Soldat> soldats;
    /**
     * Les coordonnées de la case des soldats au début du tour.
     */
    private int xActuel, yActuel;
    /**
     * Les coordonnées de la case à atteindre à la fin du tour actuel.
     */
    private int xSuivant, ySuivant;
    /**
     * La vitesse des soldats appliquant l'ordre (égal à la vitesse du soldat le moins rapide de la liste des soldats).
     */
    private int vitesse;

    /**
     * Ajoute un ordre au château origine.
     * @param cible Le château cible de l'ordre.
     * @param origine Le château origine de l'ordre.
     * @param soldats La liste des soldats appliquant l'ordre.
     */
    public Ordre(Chateau cible, Chateau origine, List<Soldat> soldats) {
        this.cible = cible;
        this.origine = origine;
        this.soldats = soldats;
        this.xActuel = origine.getCaseX();
        this.yActuel = origine.getCaseY();
        this.xSuivant = origine.entreeChateau()[0];
        this.ySuivant = origine.entreeChateau()[1];
        majVitesse();
    }

    /**
     * Met à jour la vitesse des soldats de l'ordre.
     */
    private void majVitesse() {
        this.vitesse = soldats.get(0).getVitesse();
        for (Soldat soldat : soldats) {
            if (soldat.getVitesse() < this.vitesse)
                this.vitesse = soldat.getVitesse();
        }
    }

    /**
     * Supprime un soldat de la liste des soldats appliquant l"ordre.
     * @param s Le soldat à supprimer.
     */
    private void supprimerSoldat(Soldat s) {
        soldats.remove(s);
        if (soldats.size() > 0)
            majVitesse();
    }

    /**
     * Actualise la case suivante à atteindre à la fin du tour.
     */
    public void caseSuivante() {

        int casesRestantes = this.vitesse;
        int xCible = cible.entreeChateau()[0], yCible = cible.entreeChateau()[1];
        int tmpX = xSuivant, tmpY = ySuivant;

        if (xSuivant == xCible && ySuivant == yCible) {
            xSuivant = cible.getCaseX();
            ySuivant = cible.getCaseY();
        } else if (xSuivant-xCible > casesRestantes) {
            this.xSuivant -= casesRestantes;
        } else if (xCible-xSuivant > casesRestantes) {
            this.xSuivant += casesRestantes;
        } else {
            casesRestantes -= Math.abs(xSuivant-xCible);
            this.xSuivant = xCible;
            if (ySuivant - yCible > casesRestantes) {
                this.ySuivant -= casesRestantes;
            } else if (yCible-ySuivant > casesRestantes) {
                this.ySuivant += casesRestantes;
            } else {
                this.ySuivant = yCible;
            }
        }
        xActuel = tmpX;
        yActuel = tmpY;

    }

    /**
     * Indique si les soldats composant l'ordre ont atteint la cible.
     * @return true si la cible est atteinte, false sinon.
     */
    public boolean cibleAtteinte() {
        return soldats.get(0).getCaseX() == cible.getCaseX() && soldats.get(0).getCaseY() == cible.getCaseY();
    }

    /**
     * Applique l'ordre lorsque la cible est atteinte (les soldats attaquent le château ou rejoignent sa réserve en fonction de son propriétaire).
     */
    public void prendreCible() {
        if (cible.getDuc() == origine.getDuc()) {
            for (Soldat s : soldats) {
                cible.ajouterSoldat(s);
            }
        } else {
            for (int i = soldats.size()-1 ; i >= 0 ; i--) {
                while (soldats.get(i).getDegats() > 0 && !cible.armeeVide()) {
                    cible.prendDegat();
                    soldats.get(i).setDegats(soldats.get(i).getDegats() - 1);
                }
                if (soldats.get(i).getDegats() == 0) {
                    soldats.get(i).removeFromLayer();
                    supprimerSoldat(soldats.get(i));
                }
            }
            if (cible.armeeVide() && this.nombreSoldats() > 0) {
                cible.changerProprietaire(origine.getDuc());
                for (Soldat s : soldats) {
                    cible.ajouterSoldat(s);
                }
            }
        }
        origine.supprimerOrdre(this);
    }

    public List<Soldat> getSoldats() {
        return soldats;
    }

    /**
     * Indique le nombre de soldats appliquant l'ordre.
     * @return La taille de la liste des soldats.
     */
    private int nombreSoldats() {
        return soldats.size();
    }

    public void setCible(Chateau cible) {
        this.cible = cible;
    }

    public int getxActuel() {
        return xActuel;
    }

    public int getyActuel() {
        return yActuel;
    }

    public int getxSuivant() {
        return xSuivant;
    }

    public int getySuivant() {
        return ySuivant;
    }

    @Override
    public String toString() {
        if (soldats.size() == 1) {
            return "1 soldat -> " + cible.getNom() + "\n";
        }
        return soldats.size() + " soldats -> " + cible.getNom() + "\n";
    }
}

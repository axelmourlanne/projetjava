package Game.Chateaux;

import java.io.Serializable;

public class Production implements Serializable {
    /**
     * Le type de production en cours.
     */
    private TypeProduction type;
    /**
     * Le nombre de tours restants avant la fin de la production.
     */
    private int tempsRestant;

    /**
     * Crée une production.
     * @param type Le type de la production.
     * @param tempsRestant Le nombre de tours avant la fin de la production.
     */
    public Production(TypeProduction type, int tempsRestant) {
        this.type = type;
        this.tempsRestant = tempsRestant;
    }

    /**
     * Diminue le temps restant de 1.
     */
    public void avancer() {
        this.tempsRestant--;
    }

    /**
     * Indique si la production est terminée.
     * @return true si elle est terminée, false sinon.
     */
    public boolean estTermine() {
        return tempsRestant == 0;
    }

    public TypeProduction getType() {
        return type;
    }

    public int getTempsRestant() {
        return tempsRestant;
    }

    @Override
    public String toString() {
        String s;
        if (type == TypeProduction.CHATEAU)
            s = "Amélioration du château";
        else if (type == TypeProduction.PIQUIER)
            s = "Piquier";
        else if (type == TypeProduction.CHEVALIER)
            s = "Chevalier";
        else
            s = "Onagre";
        return s + "\n";
    }
}

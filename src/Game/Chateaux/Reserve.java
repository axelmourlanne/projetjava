package Game.Chateaux;

import Game.Soldats.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Reserve implements Serializable {
    /**
     * La liste des piquiers appartenant à la réserve.
     */
    private List<Piquier> piquiers = new ArrayList<>();
    /**
     * La liste des chevaliers appartenant à la réserve.
     */
    private List<Chevalier> chevaliers = new ArrayList<>();
    /**
     * La liste des onagres appartenant à la réserve;
     */
    private List<Onagre> onagres = new ArrayList<>();

    /**
     * Initialise la réserve avec le nombre de soldats indiqués en paramètre.
     * @param chateau Le château auquel appartient la réserve.
     * @param nbPiquiers Le nombre de piquiers de départ.
     * @param nbChevaliers Le nombre de chevaliers de départ.
     * @param nbOnagres Le nombre d'onagres de départ.
     */
    Reserve(Chateau chateau, int nbPiquiers, int nbChevaliers, int nbOnagres) {
        for (int i = 0 ; i < nbPiquiers ; i++) {
            Piquier p = new Piquier(chateau);
            ajouterPiquier(p);
        }
        for (int i = 0 ; i < nbChevaliers ; i++) {
            Chevalier c = new Chevalier(chateau);
            ajouterChevalier(c);
        }
        for (int i = 0 ; i < nbOnagres ; i++) {
            Onagre o = new Onagre(chateau);
            ajouterOnagre(o);
        }
    }

    /**
     * Ajoute un piquier à la réserve.
     * @param p Le piquier à ajouter.
     */
    public void ajouterPiquier(Piquier p) {
        piquiers.add(p);
        p.removeFromLayer();
    }
    /**
     * Ajoute un chevalier à la réserve.
     * @param c Le chevalier à ajouter.
     */
    public void ajouterChevalier(Chevalier c) {
        chevaliers.add(c);
        c.removeFromLayer();
    }
    /**
     * Ajoute un onagre à la réserve.
     * @param o L'onagre à ajouter.
     */
    public void ajouterOnagre(Onagre o) {
        onagres.add(o);
        o.removeFromLayer();
    }

    /**
     * Supprime un piquier de la réserve.
     * @param p Le soldat à supprimer.
     */
    public void supprimerPiquier(Piquier p) {
        piquiers.remove(p);
    }
    /**
     * Supprime un chevalier de la réserve.
     * @param c Le chevalier à supprimer.
     */
    public void supprimerChevalier(Chevalier c) {
        chevaliers.remove(c);
    }
    /**
     * Supprime un onagre de la réserve.
     * @param o L'onagre à supprimer.
     */
    public void supprimerOnagre(Onagre o) {
        onagres.remove(o);
    }

    /**
     * Indique si la réserve de soldats est vide.
     * @return true si la réserve est vide, false sinon.
     */
    public boolean reserveVide() {
        return nombrePiquiers()+nombreChevaliers()+nombreOnagres() == 0;
    }

    /**
     * Sélectionne un soldat de la réserve pris aléatoirement.
     * @return Le soldat sélectionné.
     */
    public Soldat soldatAleatoire() {
        if (reserveVide())
            return null;
        while (true) {
            int n = (int) (Math.random()*3);
            if (n == 0 && piquiers.size() > 0)
                return piquiers.get(0);
            else if (n == 1 && chevaliers.size() > 0)
                return chevaliers.get(0);
            else if (onagres.size() > 0)
                return onagres.get(0);
        }
    }

    /**
     * Indique le nombre de piquiers présents dans la réserve.
     * @return la taille de la liste des piquiers.
     */
    public int nombrePiquiers() {
        return piquiers.size();
    }

    /**
     * Indique le nombre de chevaliers présents dans la réserve.
     * @return La taille de la liste des chevaliers.
     */
    public int nombreChevaliers() {
        return chevaliers.size();
    }

    /**
     * Indique le nombre d'onagres présents dans la réserve.
     * @return La taille de la liste des onagres.
     */
    public int nombreOnagres() {
        return onagres.size();
    }

    public List<Piquier> getPiquiers() {
        return piquiers;
    }

    public List<Chevalier> getChevaliers() {
        return chevaliers;
    }

    public List<Onagre> getOnagres() {
        return onagres;
    }

    @Override
    public String toString() {
        return + nombrePiquiers() + " piquier(s), " + nombreChevaliers() + " chevalier(s), " + nombreOnagres() + " onagre(s)\n";
    }
}

package Game.Chateaux;

import java.io.Serializable;

public enum TypeProduction implements Serializable {
    /**
     * La production d'un piquier.
     */
    PIQUIER,
    /**
     * La production d'un chevalier.
     */
    CHEVALIER,
    /**
     * La production d'un onagre.
     */
    ONAGRE,
    /**
     * La production d'un niveau supérieur du château.
     */
    CHATEAU
}

package Game;

import Game.Chateaux.Chateau;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Duc implements Serializable {
    /**
     * Le nom du duc.
     */
    private String nom;
    /**
     * Le numéro du duc.
     */
    private int numero;
    /**
     * La liste des châteaux possédés par le duc.
     */
    private List<Chateau> chateaux = new ArrayList<>();

    /**
     * Crée un duc.
     * @param nom Le nom du duc.
     * @param numero Le numéro du duc.
     * @param origine Le château d'origine du duc.
     */
    public Duc(String nom, int numero, Chateau origine) {
        this.nom = nom;
        this.numero = numero;
        chateaux.add(origine);
    }

    /**
     * Ajoute un château à la liste des châteaux possédés par le duc.
     * @param c le château à ajouter.
     */
    public void ajouterChateau(Chateau c) {
        this.chateaux.add(c);
    }

    /**
     * Supprime un château de la liste des châteaux possédés par le duc.
     * @param c Le château à supprimer.
     */
    public void supprimerChateau(Chateau c) {
        this.chateaux.remove(c);
    }

    public String getNom() {
        return nom;
    }

    public int getNumero() {
        return numero;
    }

    public List<Chateau> getChateaux() {
        return chateaux;
    }
}

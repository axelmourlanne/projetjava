package Game;

import java.io.*;
import java.util.*;

import Game.Chateaux.Chateau;
import Game.Chateaux.Ordre;
import Game.Chateaux.Production;
import Game.Chateaux.TypeProduction;
import Game.Soldats.Chevalier;
import Game.Soldats.Onagre;
import Game.Soldats.Piquier;
import Game.Soldats.Soldat;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class Main extends Application {

	public static Pane playfieldLayer;

	/**
	 * Le duc représentant le joueur.
	 */
	public static Duc joueur;
	/**
	 * La liste des châteaux du jeu.
	 */
	public static List<Chateau> chateaux = new ArrayList<>();

	/**
	 * Le titre de la barre d'état.
	 */
	private Text titre = new Text();
	/**
	 * Le contenu de la barre d'état (en dehors du titre).
	 */
	private Text infos = new Text();
	/**
	 * Les coordonnées du dernier clic sur la grille de jeu.
	 */
	private int dernierX, dernierY;

	private Scene scene;
	private Input input;
	private AnimationTimer gameLoop;

	private boolean gamePaused = false;
	private boolean pauseButtonPushed = false;

	private long tempsDebut = System.currentTimeMillis();

	Group root;

	@Override
	public void start(Stage primaryStage) {

		root = new Group();
		scene = new Scene(root, Settings.SCENE_WIDTH+Settings.STATUS_BAR_WIDTH, Settings.SCENE_HEIGHT);
		scene.getStylesheets().add(getClass().getResource("/css/application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
		primaryStage.setTitle("Dukes of the Realm");

		// create layers
		playfieldLayer = new Pane();
		root.getChildren().add(playfieldLayer);

		loadGame();

		gameLoop = new AnimationTimer() {
			@Override
			public void handle(long now) {
				processInput(input, now);

				if (!gamePaused) {
					update();
				}

				//update score, health, etc
				//update();

			}

			private void processInput(Input input, long now) {
				if (input.isExit()) {

					// Sauvegarde du jeu
					try {
						ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("sauvegarde"));
						o.writeObject(joueur);
						for (Chateau c : chateaux) {
							o.writeObject(c);
						}
						o.close();
					} catch (IOException e) {
						e.printStackTrace();
					}

					Platform.exit();
					System.exit(0);
				} else if (input.isPause()) {
					if (!pauseButtonPushed) {
						pauseButtonPushed = true;
						gamePaused = !gamePaused;
						if (!gamePaused)
							tempsDebut = System.currentTimeMillis();
					}
				} else {
					pauseButtonPushed = false;
				}

			}

		};
		gameLoop.start();
	}

	/**
	 * Charge les différents éléments du jeu.
	 */
	private void loadGame() {
		input = new Input(scene);
		input.addListeners();

		creerBarreEtat();
		creerGrille();

		// Chargement de la sauvegarde si elle existe
		try {
			ObjectInputStream o = new ObjectInputStream(new FileInputStream("sauvegarde"));
			joueur = (Duc) o.readObject();
			for (int i = 0 ; i < Settings.NB_CHATEAUX+Settings.NB_CHATEAUX_NEUTRES ; i++) {
				Chateau c = (Chateau) o.readObject();
				c.setLayer(playfieldLayer);
				if (i < Settings.NB_CHATEAUX) {
					Image image = new Image(getClass().getResource(Settings.IMAGES_CHATEAUX[i]).toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true);
					c.setImageView(new ImageView(image));
					c.getImageView().relocate(c.getX(), c.getY());
					c.getImageView().setRotate(90*c.getOrientation().ordinal());
				} else {
					Image image = new Image(getClass().getResource("/images/chateauNeutre.png").toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true);
					c.setImageView(new ImageView(image));
					c.getImageView().relocate(c.getX(), c.getY());
					c.getImageView().setRotate(90*c.getOrientation().ordinal());
				}
				chateaux.add(c);
				c.addToLayer();
				for (Piquier p : c.getReserve().getPiquiers()) {
					p.setLayer(playfieldLayer);
					Image image = new Image(getClass().getResource(Settings.IMAGES_SOLDATS[i]).toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true);
					p.setImageView(new ImageView(image));
					p.getImageView().relocate(p.getX(), p.getY());
					p.getImageView().setRotate(0);
				}
				for (Chevalier ch : c.getReserve().getChevaliers()) {
					ch.setLayer(playfieldLayer);
					Image image = new Image(getClass().getResource(Settings.IMAGES_SOLDATS[i]).toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true);
					ch.setImageView(new ImageView(image));
					ch.getImageView().relocate(ch.getX(), ch.getY());
					ch.getImageView().setRotate(0);
				}
				for (Onagre on : c.getReserve().getOnagres()) {
					on.setLayer(playfieldLayer);
					Image image = new Image(getClass().getResource(Settings.IMAGES_SOLDATS[i]).toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true);
					on.setImageView(new ImageView(image));
					on.getImageView().relocate(on.getX(), on.getY());
					on.getImageView().setRotate(0);
				}
				for (Ordre ordre : c.getOrdres()) {
					for (Soldat s : ordre.getSoldats())
						s.setLayer(playfieldLayer);
					Soldat s = ordre.getSoldats().get(0);
					Image image = new Image(getClass().getResource(Settings.IMAGES_SOLDATS[i]).toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true);
					s.setImageView(new ImageView(image));
					s.getImageView().relocate(s.getX(), s.getY());
					s.getImageView().setRotate(0);
					s.addToLayer();
				}
			}
			o.close();
		} catch (FileNotFoundException e) {
			creerChateaux();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		scene.setOnMousePressed(e -> clicGauche((int)(e.getX()/Settings.TAILLE_CASE), (int)(e.getY()/Settings.TAILLE_CASE)));
		scene.setOnContextMenuRequested(e -> clicDroit(e, (int)(e.getX()/Settings.TAILLE_CASE), (int)(e.getY()/Settings.TAILLE_CASE)));
	}

	/**
	 * Affiche les cases sur la fenêtre.
	 */
	private void creerGrille() {
		for (int x = Settings.TAILLE_CASE; x < Settings.SCENE_WIDTH ; x += Settings.TAILLE_CASE) {
			Line l = new Line(x, 0, x, Settings.SCENE_HEIGHT);
			l.setStrokeWidth(1);
			l.setStroke(Color.color(0, 0.5,0));
			l.setOpacity(0.3);
			root.getChildren().addAll(l);
		}
		for (int y = Settings.TAILLE_CASE; y < Settings.SCENE_WIDTH ; y += Settings.TAILLE_CASE) {
			Line l = new Line(0, y, Settings.SCENE_WIDTH, y);
			l.setStrokeWidth(1);
			l.setStroke(Color.GREEN);
			l.setOpacity(0.3);
			root.getChildren().addAll(l);
		}
	}

	/**
	 * Crée les châteaux et les affiche sur la fenêtre.
	 */
	private void creerChateaux() {

		for (int i = 0; i < Settings.NB_CHATEAUX+Settings.NB_CHATEAUX_NEUTRES; i++) {

			int caseX = 0, caseY = 0;
			boolean xyValide = false;
			while (!xyValide) {
				caseX = (int) (Math.random()*((Settings.SCENE_WIDTH/Settings.TAILLE_CASE)-2))+1;
				caseY = (int) (Math.random()*((Settings.SCENE_HEIGHT/Settings.TAILLE_CASE)-2))+1;
				xyValide = coordonneesValides(caseX,caseY);
			}
			int orientation = (int) (Math.random()*4);
			Image castleImage = new Image(getClass().getResource(Settings.IMAGES_CHATEAUX[i]).toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true);
			boolean neutre = false;
			if (i >= Settings.NB_CHATEAUX) {
				neutre = true;
				castleImage = new Image(getClass().getResource("/images/chateauNeutre.png").toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true);
			}

			Chateau c = new Chateau(playfieldLayer, castleImage, neutre, Settings.NOMS_CHATEAUX[i], null, caseX, caseY, Orientation.values()[orientation]);
			Duc d = new Duc(Settings.NOMS_DUCS[i], i, c);
			c.setDuc(d);
			c.creerReserve(neutre);
			if (i == 0) joueur = d;
			chateaux.add(c);
		}
	}

	/**
	 * Teste si le château à ajouter n'est pas trop proche d'un château déjà ajouté.
	 * @param caseX La coordonnée x du château.
	 * @param caseY La coordonnée y du château.
	 * @return true si les coordonnées sont valides (càd si le château n'est pas trop proche des autres), false sinon.
	 */
	private boolean coordonneesValides(int caseX, int caseY) {
		if (chateaux.isEmpty()) return true;
		for (Chateau c : chateaux) {
			int distMin = Settings.DISTANCE_MINIMALE;
			int x2 = c.getCaseX(), y2 = c.getCaseY();
			if (caseX-x2 < distMin && x2-caseX < distMin && caseY-y2 < distMin && y2-caseY < distMin) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Affiche la barre d'état sur le côté de la fenêtre.
	 */
	private void creerBarreEtat() {
		ScrollPane statusPane = new ScrollPane();
		statusPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
		statusPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
		VBox statusBar = new VBox();
		afficherCommandes();
		infos.getStyleClass().add("infos");
		statusBar.getChildren().addAll(titre, infos);
		statusBar.getStyleClass().add("statusBar");
		statusBar.relocate(Settings.SCENE_WIDTH, 0);
		statusBar.setPrefSize(Settings.STATUS_BAR_WIDTH, Settings.SCENE_HEIGHT);
		statusPane.relocate(Settings.SCENE_WIDTH, 0);
		statusPane.setContent(statusBar);
		root.getChildren().addAll(statusPane);
	}

	/**
	 * Affiche les informations demandées en cas de clic gauche sur la grille de jeu.
	 * @param caseX La coordonnée x du clic.
	 * @param caseY La coordonnée y du clic.
	 */
	private void clicGauche(int caseX, int caseY) {
		dernierX = caseX;
		dernierY = caseY;
		for (Chateau c : chateaux) {
			if (c.getCaseX() == caseX && c.getCaseY() == caseY) {
			    titre.setText(c.getNom());
				infos.setText(c.toString(c.getDuc() == joueur));
				return;
			}
		}
		afficherCommandes();
	}

	/**
	 * Affiche le menu en cas de clic droit sur la grille.
	 * @param e L'évènement correspondant au clic.
	 * @param caseX La coordonnée x du clic.
	 * @param caseY La coordonnée y du clic.
	 */
	private void clicDroit(ContextMenuEvent e, int caseX, int caseY) {

		dernierX = caseX;
		dernierY = caseY;

		for (Chateau c : chateaux) {

			if (c.getCaseX() == caseX && c.getCaseY() == caseY) {

				ContextMenu contextMenu = new ContextMenu();
				MenuItem renommer = new MenuItem("Renommer...");
				renommer.setOnAction(evt -> renommer(c));
				contextMenu.getItems().add(renommer);
				if (c.getDuc() != joueur) {
					Menu attaque = new Menu("Attaquer depuis");
					for (int i = 0 ; i < joueur.getChateaux().size() ; i++) {
						Chateau origine = joueur.getChateaux().get(i);
						MenuItem m = new MenuItem(origine.getNom());
						m.setOnAction(evt -> selectionnerSoldats(origine, c));
						attaque.getItems().add(m);
					}
					contextMenu.getItems().add(attaque);

				} else {

                    SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();
				    Menu produire = new Menu("Produire");
				    MenuItem chateau = new MenuItem("Amélioration du château (" + c.getCoutProduction() + " florins)");
				    MenuItem piquier = new MenuItem("Piquier (" + Piquier.coutProduction + " florins)");
				    MenuItem chevalier = new MenuItem("Chevalier (" + Chevalier.coutProduction + " florins)");
				    MenuItem onagre = new MenuItem("Onagre (" + Onagre.coutProduction + " florins)");
				    chateau.setOnAction(evt -> ajouterProduction(c, TypeProduction.CHATEAU));
                    piquier.setOnAction(evt -> ajouterProduction(c, TypeProduction.PIQUIER));
                    chevalier.setOnAction(evt -> ajouterProduction(c, TypeProduction.CHEVALIER));
                    onagre.setOnAction(evt -> ajouterProduction(c, TypeProduction.ONAGRE));
                    produire.getItems().addAll(chateau, piquier, chevalier, onagre);
                    contextMenu.getItems().addAll(separatorMenuItem, produire);

                    if (c.nombreProductions() > 0) {
                        MenuItem annulerProd = new MenuItem("Annuler une production...");
                        annulerProd.setOnAction(evt -> annulerProduction(c));
                        contextMenu.getItems().add(annulerProd);
                    }

					if (joueur.getChateaux().size() > 1) {
						Menu transfert =  new Menu("Transférer depuis");
						for (int i = 0 ; i < joueur.getChateaux().size() ; i++) {
							Chateau origine = joueur.getChateaux().get(i);
							if (c != origine) {
								MenuItem m = new MenuItem(origine.getNom());
								m.setOnAction(evt -> selectionnerSoldats(origine, c));
								transfert.getItems().add(m);
							}
						}
						contextMenu.getItems().add(transfert);
					}

                    if (c.nombreOrdres() > 0) {
                        MenuItem annulerOrdre = new MenuItem("Annuler un ordre...");
                        annulerOrdre.setOnAction(evt -> annulerOrdre(c));
                        contextMenu.getItems().add(annulerOrdre);
                    }
                }

				contextMenu.show(c.getView(), e.getScreenX(), e.getScreenY());
				return;
			}
		}
	}

	/**
	 * Passe au tour suivant.
	 */
	private void tourSuivant() {
	    for (Chateau c : chateaux) {
	        c.effectuerTour();
        }
	    clicGauche(dernierX, dernierY);
    }

	/**
	 * Crée une alerte.
	 * @param type Le type de l'alerte.
	 * @param titre Le titre de la fenêtre d'alerte.
	 * @param message Le contenu de la fenêtre d'alerte.
	 * @return L'alerte créée.
	 */
	private Alert alerte(Alert.AlertType type, String titre, String message) {
        Alert alert = new Alert(type);
        alert.setTitle(titre);
        alert.setContentText(null);
        alert.setHeaderText(message);
        alert.getButtonTypes().clear();
        alert.getButtonTypes().add(new ButtonType("OK", ButtonBar.ButtonData.OK_DONE));
        return alert;
    }

	/**
	 * Affiche la boîte de dialogue pour sélectionner le nombre de soldats.
	 * @param typeSoldat Le type de soldat à afficher.
	 * @param origine Le château origine de l'ordre.
	 * @param cible Le château cible de l'ordre.
	 * @return Le nombre de soldats sélectionnés.
	 */
    private int boiteSelectionSoldats(String typeSoldat, Chateau origine, Chateau cible) {
		TextInputDialog dialog = new TextInputDialog("0");
		dialog.setTitle("Envoyer des soldats");
		dialog.setContentText("Nombre de " + typeSoldat +" à envoyer :");
		dialog.setHeaderText("Votre réserve contient " + origine.getReserve() +
				"\nLa réserve de la cible contient " + cible.getReserve());
		Optional<String> result = dialog.showAndWait();
		final int[] resultat = {0};
		result.ifPresent(nb -> {
			try {
				int n = Integer.parseInt(nb);
				if (n < 0) n = 0;
				resultat[0] = Math.min(n, origine.getReserve().nombrePiquiers());
			} catch (NumberFormatException e) {
				Alert alert = alerte(Alert.AlertType.ERROR, "Envoyer des soldats", "Vous devez entrer un nombre.");
				alert.showAndWait();
				boiteSelectionSoldats(typeSoldat, origine, cible);
			}
		});
		return resultat[0];
	}

	/**
	 * Ajoute une production au château sélectionné.
	 * @param c Le château sélectionné.
	 * @param type Le type de production.
	 */
    private void ajouterProduction(Chateau c, TypeProduction type) {
	    if (c.ajouterProduction(type)) {
			clicGauche(c.getCaseX(), c.getCaseY());
        } else {
			Alert alert = alerte(Alert.AlertType.INFORMATION, "Trésor insuffisant", "Vous ne possédez pas assez de florins pour réaliser cette production.");
			alert.showAndWait();
		}
    }

	/**
	 * Sélectionne des soldats à envoyer.
	 * @param origine Le château d'origine de l'ordre.
	 * @param cible Le château cible de l'ordre.
	 */
	private void selectionnerSoldats(Chateau origine, Chateau cible) {
        if (origine.armeeVide()) {
            Alert alert = alerte(Alert.AlertType.INFORMATION, "Réserve vide", "Impossible d'envoyer des soldats : la réserve est vide.");
            alert.showAndWait();
        } else {
        	int piquiers = 0, chevaliers = 0, onagres = 0;
        	if (origine.getReserve().nombrePiquiers() > 0)
				piquiers = Math.min(selectionnerPiquiers(origine, cible), origine.getReserve().nombrePiquiers());
        	if (origine.getReserve().nombreChevaliers() > 0)
        		chevaliers = Math.min(selectionnerChevaliers(origine, cible), origine.getReserve().nombreChevaliers());
        	if (origine.getReserve().nombreOnagres() > 0)
        		onagres = Math.min(selectionnerOnagres(origine, cible), origine.getReserve().nombreOnagres());
        	if (piquiers > 0 || chevaliers > 0 || onagres > 0)
        		origine.ajouterOrdre(cible, piquiers, chevaliers, onagres);
		}
	}

	/**
	 * Sélectionne des piquiers à envoyer.
	 * @param origine Le château origine de l'ordre.
	 * @param cible Le château cible de l'ordre.
	 * @return Le nombre de piquiers sélectionnés.
	 */
    private int selectionnerPiquiers(Chateau origine, Chateau cible) {
	    return boiteSelectionSoldats("PIQUIERS", origine, cible);
    }
	/**
	 * Sélectionne des chevaliers à envoyer.
	 * @param origine Le château origine de l'ordre.
	 * @param cible Le château cible de l'ordre.
	 * @return Le nombre de chevaliers sélectionnés.
	 */
    private int selectionnerChevaliers(Chateau origine, Chateau cible) {
		return boiteSelectionSoldats("CHEVALIERS", origine, cible);
    }
	/**
	 * Sélectionne des onagres à envoyer.
	 * @param origine Le château origine de l'ordre.
	 * @param cible Le château cible de l'ordre.
	 * @return Le nombre d'onagres sélectionnés.
	 */
    private int selectionnerOnagres(Chateau origine, Chateau cible) {
		return boiteSelectionSoldats("ONAGRES", origine, cible);
    }

	/**
	 * Annule une production du château sélectionné.
	 * @param c Le château sélectionné.
	 */
	private void annulerProduction(Chateau c) {
        ChoiceDialog<Production> dialog = new ChoiceDialog<>(c.getProductions().get(0));
        dialog.setTitle("Annuler une production");
        dialog.setHeaderText("Choisissez la production à annuler.\nNote : les productions ci-dessous sont affichées\n\t   dans l'ordre de la file de productions.");
        dialog.setContentText("Production :");
        for (int i = 0 ; i < c.nombreProductions() ; i++) {
            dialog.getItems().add(c.getProductions().get(i));
        }
        Optional<Production> result = dialog.showAndWait();
        result.ifPresent(p -> {
        	c.annulerProduction(p);
			clicGauche(c.getCaseX(), c.getCaseY());
		});
    }
	/**
	 * Annule un ordre du château sélectionné.
	 * @param c Le château sélectionné.
	 */
    private void annulerOrdre(Chateau c) {
        ChoiceDialog<Ordre> dialog = new ChoiceDialog<>(c.getOrdres().get(0));
        dialog.setTitle("Annuler un ordre");
        dialog.setHeaderText("Choisissez l'ordre à annuler.");
        dialog.setContentText("Ordre :");
        for (int i = 0 ; i < c.nombreOrdres() ; i++) {
            dialog.getItems().add(c.getOrdres().get(i));
        }
        Optional<Ordre> result = dialog.showAndWait();
		result.ifPresent(o -> {
			c.annulerOrdre(o);
			clicGauche(c.getCaseX(), c.getCaseY());
		});
    }

	/**
	 * Affiche la fenêtre de renommage du château.
	 * @param c Le château à renommer.
	 */
	private void renommer(Chateau c) {
		TextInputDialog dialog = new TextInputDialog(c.getNom());
		dialog.setTitle("Renommer le château");
		dialog.setContentText("Nouveau nom :");
		dialog.setHeaderText(null);
		Optional<String> result = dialog.showAndWait();
		result.ifPresent(c::setNom);
		clicGauche(c.getCaseX(), c.getCaseY());
	}

	/**
	 * Affiche les commandes du jeu dans la barre d'état.
	 */
	private void afficherCommandes() {
		String s = "Clic gauche : \n" +
				"Informations sur le château\n\n" +
				"Clic droit :\n" +
				"Renommer le château /\n" +
                "Ajouter une production /\n" +
				"Attaquer le château /\n" +
				"Transférer des troupes\n\n" +
				"P ou Espace : \n" +
				"Mettre le jeu en pause\n\n" +
				"Echap :\n" +
				"Quitter le jeu\n\n" +
                "Vos châteaux ont la porte rouge";
		infos.setText(s);
		titre.setText("Commandes\n");
	}

	/**
	 * Affiche le message de défaite.
	 */
	private void defaite() {
		HBox hbox = new HBox();
		hbox.setPrefSize(Settings.SCENE_WIDTH, Settings.SCENE_HEIGHT);
		hbox.getStyleClass().add("message");
		Text message = new Text();
		message.getStyleClass().add("message");
		message.setText("Game over !");
		hbox.getChildren().add(message);
		root.getChildren().add(hbox);
		gameLoop.stop();
	}
	/**
	 * Affiche le message de victoire.
	 */
	private void victoire() {
		HBox hbox = new HBox();
		hbox.setPrefSize(Settings.SCENE_WIDTH, Settings.SCENE_HEIGHT);
		hbox.getStyleClass().add("message");
		Text message = new Text();
		message.getStyleClass().add("message");
		message.setText("Vous avez conquis le royaume !");
		hbox.getChildren().add(message);
		root.getChildren().add(hbox);
		gameLoop.stop();
	}

	/**
	 * Met à jour les différents sprites du jeu.
	 */
	private void update() {
		if (joueur.getChateaux().size() == 0) {
			defaite();
			return;
		}
		if (joueur.getChateaux().size() == Settings.NB_CHATEAUX_NEUTRES+Settings.NB_CHATEAUX) {
			victoire();
			return;
		}
		long diffTemps = System.currentTimeMillis() - tempsDebut;
		for (Chateau c : chateaux) {
			for (Ordre o : c.getOrdres()) {
				Soldat s = o.getSoldats().get(0);
				if (!playfieldLayer.getChildren().contains(s.getView()))
					s.addToLayer();
				int x = s.getX(), y = s.getY();
				if (o.getxSuivant() * Settings.TAILLE_CASE - x > 0) {
					s.setCaseX(o.getxActuel());
					s.setX(s.getX() + (int) ((Math.abs(o.getxActuel()-o.getxSuivant()))*Settings.TAILLE_CASE*diffTemps/Settings.DUREE_TOUR));
				} if (o.getxSuivant() * Settings.TAILLE_CASE - x < 0) {
					s.setCaseX(o.getxActuel());
					s.setX(s.getX() - (int) ((Math.abs(o.getxActuel()-o.getxSuivant()))*Settings.TAILLE_CASE*diffTemps/Settings.DUREE_TOUR));
				} if (o.getySuivant() * Settings.TAILLE_CASE - y > 0) {
					s.setCaseY(o.getyActuel());
					s.setY(s.getY() + (int) ((Math.abs(o.getyActuel()-o.getySuivant()))*Settings.TAILLE_CASE*diffTemps/Settings.DUREE_TOUR));
				} if (o.getySuivant() * Settings.TAILLE_CASE - y < 0) {
					s.setCaseY(o.getyActuel());
					s.setY(s.getY() - (int) ((Math.abs(o.getyActuel()-o.getySuivant()))*Settings.TAILLE_CASE*diffTemps/Settings.DUREE_TOUR));
				}
				s.updateUI();
				for (Soldat so : o.getSoldats()) {
					so.setX(s.getX());
					so.setY(s.getY());
				}
			}
		}
		if (diffTemps > Settings.DUREE_TOUR) {
			tourSuivant();
			tempsDebut = System.currentTimeMillis();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

}
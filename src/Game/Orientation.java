package Game;

import java.io.Serializable;

public enum Orientation implements Serializable {
    SUD, OUEST, NORD, EST
}

package Game;

public class Settings {

    /**
     * La largeur de la grille de jeu.
     * Note : afin d'éviter les problèmes d'affichages, SCENE_WIDTH doit être un multiple de {@link #TAILLE_CASE TAILLE_CASE}.
     */
	public static final int SCENE_WIDTH = 1000;
    /**
     * La hauteur de la grille de jeu.
     * Note : afin d'éviter les problèmes d'affichages, SCENE_HEIGHT doit être un multiple de {@link #TAILLE_CASE TAILLE_CASE}.
     */
    public static final int SCENE_HEIGHT = 600;
    /**
     * La largeur du menu.
     */
	public static final int STATUS_BAR_WIDTH = 300;
    /**
     * La largeur d'une case de la grille (en pixels).
     */
    public static final int TAILLE_CASE = 25;
    /**
     * Le nombre minimal de cases entre chaque château.
     */
    public static final int DISTANCE_MINIMALE = 6;
    /**
     * Le nombre de châteaux présents dans le jeu.
     * Note : ce nombre ne doit pas être supérieur à 15.
     * Note : ce nombre ne doit pas être supérieur à 8 si l'on souhaite que deux objets (châteaux ou soldats) n'appartenant pas au même duc n'aient pas la même couleur.
     */
    public static final int NB_CHATEAUX = 5;
    /**
     * Le nombre de châteaux neutres présents dans le jeu.
     * Note : ce nombre ne doit pas être supérieur à 20-{@link #NB_CHATEAUX NB_CHATEAUX}. En revanche, il peut être égal à 0.
     */
    public static final int NB_CHATEAUX_NEUTRES = 5;
    /**
     * Le nom des châteaux du jeu.
     */
    public static final String[] NOMS_CHATEAUX = {
            "Château de Roquetaillade",
            "Château de Cazeneuve",
            "Château de Villandraut",
            "Château des Quat'Sos",
            "Château de Langoiran",
            "Château de la Trave",
            "Château de Budos",
            "Château d'Aspremont",
            "Château de Marracq",
            "Château d'Abbadie",
            "Château de Nérac",
            "Château de Bellocq",
            "Château du Portalet",
            "Château de Mauléon",
            "Château de Sault",
            "Château de Bonaguil",
            "Château de Madaillan",
            "Château de Gavaudun",
            "Château du Sendat",
            "Château de Morlanne"
    };

    /**
     * Le nom des ducs du jeu.
     */
    public static final String[] NOMS_DUCS = {
            "VOUS",
            "Eudes",
            "Aliénor",
            "Hunald",
            "Waïfre",
            "Aznar",
            "Ramnulf",
            "Ebles",
            "Garcie",
            "Rebrit",
            "Gondovald",
            "Caribert",
            "Acfred",
            "Othon",
            "Gonzalve",
            "Guérech",
            "Berthe",
            "Alix",
            "Rohan",
            "Manu"
    };

    /**
     * Le lien vers l'image des différents châteaux.
     */
    public static final String[] IMAGES_CHATEAUX = {
            "/images/chateauRouge.png",
            "/images/chateauJaune.png",
            "/images/chateauVert.png",
            "/images/chateauBleu.png",
            "/images/chateauViolet.png",
            "/images/chateauOrange.png",
            "/images/chateauGris.png",
            "/images/chateauCyan.png",
            "/images/chateauJaune.png",
            "/images/chateauVert.png",
            "/images/chateauBleu.png",
            "/images/chateauViolet.png",
            "/images/chateauOrange.png",
            "/images/chateauGris.png",
            "/images/chateauCyan.png",
            "/images/chateauJaune.png",
            "/images/chateauVert.png",
            "/images/chateauBleu.png",
            "/images/chateauViolet.png",
            "/images/chateauOrange.png",
    };

    /**
     * Le lien vers l'image des soldats appartenant aux différents châteaux.
     */
    public static final String[] IMAGES_SOLDATS = {
            "/images/soldatRouge.png",
            "/images/soldatJaune.png",
            "/images/soldatVert.png",
            "/images/soldatBleu.png",
            "/images/soldatViolet.png",
            "/images/soldatOrange.png",
            "/images/soldatGris.png",
            "/images/soldatCyan.png",
            "/images/soldatJaune.png",
            "/images/soldatVert.png",
            "/images/soldatBleu.png",
            "/images/soldatViolet.png",
            "/images/soldatOrange.png",
            "/images/soldatGris.png",
            "/images/soldatCyan.png",
            "/images/soldatJaune.png",
            "/images/soldatVert.png",
            "/images/soldatBleu.png",
            "/images/soldatViolet.png",
            "/images/soldatOrange.png",
    };

    /**
     * La durée (en millisecondes) des tours de jeu.
     */
    public static final int DUREE_TOUR = 2000;

}

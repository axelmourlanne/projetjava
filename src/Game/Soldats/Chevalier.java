package Game.Soldats;

import Game.Chateaux.Chateau;

public class Chevalier extends Soldat {
    /**
     * Le nombre de tours nécessaires pour produire le chevalier.
     */
    public static final int tempsProduction = 10;
    /**
     * L'argent nécessaire pour produire le chevalier.
     */
    public static final int coutProduction = 250;

    /**
     * Crée un chevalier.
     * @param chateau Le château propriétaire du chevalier.
     */
    public Chevalier(Chateau chateau) {
        super(chateau,5, 4, 4);
    }
}

package Game.Soldats;

import Game.Chateaux.Chateau;

public class Onagre extends Soldat {
    /**
     * Le nombre de tours nécessaires pour produire l'onagre.
     */
    public static final int tempsProduction = 20;
    /**
     * L'argent nécessaire pour produire l'onagre.
     */
    public static final int coutProduction = 500;

    /**
     * Crée un onagre.
     * @param chateau Le château propriétaire de l'onagre.
     */
    public Onagre(Chateau chateau) {
        super(chateau, 1, 5, 10);
    }
}
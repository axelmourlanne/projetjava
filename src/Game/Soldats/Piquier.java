package Game.Soldats;

import Game.Chateaux.Chateau;

public class Piquier extends Soldat {
    /**
     * Le nombre de tours nécessaires pour produire le piquier.
     */
    public static final int tempsProduction = 3;
    /**
     * L'argent nécessaire pour produire le piquier.
     */
    public static final int coutProduction = 50;

    /**
     * Crée un piquier.
     * @param chateau Le château propriétaire du piquier.
     */
    public Piquier(Chateau chateau) {
        super(chateau, 2, 1, 1);
    }
}

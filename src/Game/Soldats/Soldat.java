package Game.Soldats;

import Game.Chateaux.Chateau;
import Game.Main;
import Game.Settings;
import Game.Sprite;
import javafx.scene.image.Image;

import java.io.Serializable;

public abstract class Soldat extends Sprite implements Serializable {
    /**
     * La vitesse du soldat (le nombre de cases où il peut se déplacer à chaque tour).
     */
    private int vitesse;
    /**
     * La vie du soldat (le nombre de dégâts qu'il peut recevoir avant de mourir).
     * Elle ne peut pas se régénérer.
     */
    private int vie;
    /**
     * Les dégâts que peut infliger le soldat avant de mourir (le nombre de vies qu'il peut faire perdre aux adversaires avant de mourir).
     */
    private int degats;

    /**
     * Crée un soldat.
     * @param chateauProprietaire Le château propriétaire du soldat.
     * @param vitesse La vitesse de déplacement du soldat.
     * @param vie Le nombre de points de vie du soldat.
     * @param degats Le nombre de points de dégâts du soldat.
     */
    Soldat(Chateau chateauProprietaire, int vitesse, int vie, int degats) {
        super(Main.playfieldLayer,
                new Image(Soldat.class.getResource(Settings.IMAGES_SOLDATS[chateauProprietaire.getDuc().getNumero()]).toExternalForm(), Settings.TAILLE_CASE, Settings.TAILLE_CASE, true, true),
                chateauProprietaire.getCaseX(), chateauProprietaire.getCaseY(), 0);
        this.vitesse = vitesse;
        this.vie = vie;
        this.degats = degats;
    }

    /**
     * Enlève un point de vie au soldat.
     */
    public void prendDegat() {
        this.vie -= 1;
    }

    public int getVitesse() {
        return vitesse;
    }

    public int getVie() {
        return vie;
    }

    public int getDegats() {
        return degats;
    }

    public void setDegats(int degats) {
        this.degats = degats;
    }
}

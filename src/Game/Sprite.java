package Game;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

import java.io.Serializable;

public abstract class Sprite implements Serializable {

    private transient ImageView imageView;

    private transient Pane layer;

    /**
     * Les coordonnées du sprite (par rapport aux cases).
     */
    protected int caseX, caseY;
    /**
     * Les coordonnées du sprite (par rapport aux pixels).
     */
    private int x, y;

    public Sprite(Pane layer, Image image, int caseX, int caseY, int orientation) {

        this.layer = layer;
        this.caseX = caseX;
        this.caseY = caseY;
        this.x = caseX*Settings.TAILLE_CASE;
        this.y = caseY*Settings.TAILLE_CASE;

        this.imageView = new ImageView(image);
        this.imageView.relocate(x, y);
        this.imageView.setRotate(90*orientation);

        addToLayer();

    }

    /**
     * Ajoute le sprite à la fenêtre.
     */
    public void addToLayer() {
        this.layer.getChildren().add(this.imageView);
    }

    /**
     * Supprime le sprite de la fenêtre.
     */
    public void removeFromLayer() {
        this.layer.getChildren().remove(this.imageView);
    }

    public void setImage(Image image, int orientation) {
        this.imageView.setImage(image);
        this.imageView.setRotate(90*orientation);
        this.updateUI();
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public void setLayer(Pane layer) {
        this.layer = layer;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
        this.caseX = x/Settings.TAILLE_CASE;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        this.caseY = y/Settings.TAILLE_CASE;
    }

    public int getCaseX() {
        return caseX;
    }

    public void setCaseX(int caseX) {
        this.caseX = caseX;
        this.x = caseX*Settings.TAILLE_CASE;
    }

    public int getCaseY() {
        return caseY;
    }

    public void setCaseY(int caseY) {
        this.caseY = caseY;
        this.y = caseY*Settings.TAILLE_CASE;
    }

    public ImageView getView() {
        return imageView;
    }

    /**
     * Met à jour la position du sprite sur la fenêtre.
     */
    public void updateUI() {
        imageView.relocate(x, y);
    }

    public boolean collidesWith(Sprite sprite) {
    	return getView().getBoundsInParent().intersects(sprite.getView().getBoundsInParent());
    }

}
